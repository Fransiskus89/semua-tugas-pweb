<!DOCTYPE html>
<html>
<head>
	<title>Tugas 1</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="TugasPweb.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			<a class="navbar-brand" href="#">LaundryApp<img src="Fortegreen_Crewmate.png" width="30px" height="24"></a>
			
  			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"aria-label="Toggle navigation">
    			<span class="navbar-toggler-icon"></span>
  			</button>
  			<div class="collapse navbar-collapse" id="navbarNav">
    			<ul class="navbar-nav">
      				<li class="nav-item active">
        				<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      				</li>
      				<li class="nav-item">
        				<a class="nav-link" href="#">Outlet</a>
      				</li>
      				<li class="nav-item">
        				<a class="nav-link" href="#">Transaksi</a>
      				</li>
      				<li class="nav-item">
        				<a class="nav-link" href="#">Paket</a>
      				</li>
      				<li class="nav-item">
        				<button type="button" class="btn btn-primary">Login</button>
      				</li>
    			</ul>
  			</div>
		</div>
 	</nav>
 	<!-- Awal Jumbotron -->
 	<div class="jumbotron jumbotron-fluid">
  		<div class="container">
  			<div class="row">
  				<div class="col-lg-8">
  					<h1 class="display-4">Get Acces to Unlimited Education Resources. Everywhere, Everytime!</h1>
    				<p class="lead">Premium Access to more than 10.000 resources ranging from courses. events e.t.c</p>
    				<button class="btn btn-primary">Get Access</button>
  				</div>
  			</div>
  		</div>
	</div>
	<!-- Akhir Jumbotron -->

	<!-- Services -->
	<section class="services d-flex align-items-center justify-content-center" id="services">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 mb-5">
					<h4>Services</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<img src="subrek.jpg">
					<h6>Unlimited Acces</h6>
					<p>One subscription unlimited acces</p>
				</div>
				<div class="col-lg-4">
					<img src="subrek2.jpg">
					<h6>Expert Teacher</h6>
					<p>Learn From indsutry experts who are passionate about teaching</p>
				</div>
				<div class="col-lg-4">
					<img src="subrek3.jpg">
					<h6>Learn Anywhere</h6>
					<p>Switch between your computer, tablet, or mobile device</p>
				</div>
			</div>
		</div>
	</section>
	<!-- Akhir Services -->

	<!-- stories -->
	<section class="stories d-flex align-items-center" id="stories">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<h1>Succes stories from our students worldwide !</h1>
					<p>semaj africa is an online education that delivers video courses, programs and resources for individual, advertising & anyobe looking to purse a career in digital marketing, accounting, web development, programing, multimedia, and cad design</p>
					<button class="btn btn-primary">Discover</button>
				</div>
				<div class="col-lg-8 text-center">
					<img src="MediaPlaceholders.png" width="80%">
				</div>
			</div>
		</div>
	</section>
	<!-- akhir Stories -->


	<!-- Course -->
	<section class="courses d-flex align-items-center" id="courses">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h4>Courses</h4>
				</div>
			</div>
			<div class="row pb-4">
				<div class="col-lg-12 text-center">
					<a href="">All</a>
					<a href="">Design</a>
					<a href="">Web Development</a>
					<a href="">Digital</a>
					<a href="">Photography</a>
					<a href="">Motion Graphics</a>
					<a href="">Digital Marketing</a>
				</div>
			</div>
			
			<div class="row">
				<?php
					include "koneksi.php";
					$data = mysqli_query($koneksi, "SELECT * FROM courses");
					while ($d = mysqli_fetch_array($data)) {
					?>

					<div class="col-lg-3">
						<div class="card">
							<div class="card-top text-center pt-2">
								<?php echo $d['pricing'] ?>
							</div>
							<img class="card-img-top" src="assets/img/<?php echo $d['img'] ?>" alt="Card image cap">
							<div class="card-body">
								<p><?php echo $d['description'] ?></p>
							</div>
						</div>
					</div>
					<?php
						}
					?>
			</div>
			<div class="row justify-content-center mt-5">
				<button class="btn btn-primary">Discover</button>
			</div>
			

		</div>
	</section>
	<!-- akhir Course -->

	<!-- Achievement -->
	<section class="achievement d-flex align-items-center" id="achievement">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<img src="assets/img/Achievement1.svg" alt="">
					<h1>5,679</h1>
					<p>Registered Students</p>
				</div>
				<div class="col-lg-3">
					<img src="assets/img/Achievement2.svg" alt="">
					<h1>2,679</h1>
					<p>Student has been helped to achieve their dreams</p>
				</div>
				<div class="col-lg-3">
					<img src="assets/img/Achievement3.svg" alt="">
					<h1>10,000</h1>
					<p>More than 10,000 people visits our site monthly</p>
				</div>
				<div class="col-lg-3">
					<img src="assets/img/Achievement4.svg" alt="">
					<h1>#10</h1>
					<p>Ranked among the top 10 growing online learning startups in West Africa</p>
				</div>
			</div>
		</div>
	</section>
	<!-- akhir achievement -->


	<!-- Testimoni -->
	<section class="testimonials d-flex align-items-center" id="testimonials">
		<div class="container-fluid">
		<div class="row text-center">
			<div class="col-lg-12">
				<h1>What Students Say</h1>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-lg-12">
				<p>Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist,.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3">
				<div class="card">
 					 <div class="card-body">
   						<img src="assets/img/testi.svg">
   						<p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Profesional, Freelancers and anyone.</p>
   						<div class="row">
   							<div class="col-lg-3">
   								<img src="assets/img/User-Pic.svg">
   							</div>
   							<div class="col-lg-9 pt-2">
   								<h5>Arthur Broklyn</h5>
   								<p>Categories: 3d Modelling</p>
   							</div>
   						</div>
  					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="card">
 					 <div class="card-body">
   						<img src="assets/img/testi.svg">
   						<p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Profesional, Freelancers and anyone.</p>
   						<div class="row">
   							<div class="col-lg-3">
   								<img src="assets/img/User-Pic.svg">
   							</div>
   							<div class="col-lg-9 pt-2">
   								<h5>Arthur Broklyn</h5>
   								<p>Categories: 3d Modelling</p>
   							</div>
   						</div>
  					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="card">
 					 <div class="card-body">
   						<img src="assets/img/testi.svg">
   						<p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Profesional, Freelancers and anyone.</p>
   						<div class="row">
   							<div class="col-lg-3">
   								<img src="assets/img/User-Pic.svg">
   							</div>
   							<div class="col-lg-9 pt-2">
   								<h5>Arthur Broklyn</h5>
   								<p>Categories: 3d Modelling</p>
   							</div>
   						</div>
  					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="card">
 					 <div class="card-body">
   						<img src="assets/img/testi.svg">
   						<p class="card-text pt-3 pb-3">Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Profesional, Freelancers and anyone.</p>
   						<div class="row">
   							<div class="col-lg-3">
   								<img src="assets/img/User-Pic.svg">
   							</div>
   							<div class="col-lg-9 pt-2">
   								<h5>Arthur Broklyn</h5>
   								<p>Categories: 3d Modelling</p>
   							</div>
   						</div>
  					</div>
				</div>
			</div>
		</div>
		</div>
	</section>
	<!-- Akhir Testimoni -->

	<!-- Newsletter -->
	<section class="newsletter d-flex align-items-center" id="newsletter">
		<div class="container">
			<img src="assets/img/Rectangle7.svg" class="imgs">
			<div class="row justify-content-center mt-5">
				<div class="col-lg-8">
					<h4>Subscribe to Our Newsletter</h4>
					<p>Get exclusive discounts and latest news deliverd to your inbox for free!</p>
					<button class="btn btn-primary">Start free trial</button>
				</div>
			</div>
		</div>
	</section>
	<!-- Akhir Newsletter -->

	<!-- contact -->
	<section class="contact d-flex align-items-center" id="contact">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<img src="assets/img/logo.svg" class="mb-3">
					<p>Semaj Africa is an Online education platform that delivers video courses, programs and resources</p>
					<img src="assets/img/sosmed.svg" class="mt-5">
				</div>
				<div class="col-lg-2">
					<h6>Quicklinks</h6>
					<ul>
						<li>Home</li>
						<li>Courses</li>
						<li>About Us</li>
						<li>Contact Us</li>
					</ul>
				</div>
				<div class="col-lg-3">
					<h6>Contact Us</h6>
					<ul>
						<li>(+62) 854. 854. 854</li>
						<li>Info@semajafrica.com</li>
						<li>Helios Tower 75 Tam Trinh Haong</li>
						<li>Mai - Ha Noi - Viet Nam</li>
					</ul>
				</div>
				<div class="col-lg-3">
					<h6>Terms and Conditions Faq</h6>
				</div>
			</div>
		</div>
	</section>
	<!-- akhir contact -->

	<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

</body>
</html>